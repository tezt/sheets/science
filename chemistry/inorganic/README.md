# INORGANIC CHEMISTRY

    |------------------------+------------------------------------------------------------------------------|
    | ionisation enthalpy    | energy required to remove e- of an isolated neutral gaseous atom or molecule |
    | electron gain enthalpy | energy released to gain an e- (electron)                                     |
    | electronegativity      | tendency to attract shared e-                                                |
    | electropositivity      | tendency to donate e-                                                        |
    | shielding effect       | reduction in the effective nuclear charge on the electron cloud              |
    |                        | s > p > d > f     order of shielding effect                                  |
    | inert pair effect      | tendency of the two e- s-orbital to remain unshared due to the need of       |
    |                        | higher energy to unpair them , which arises due to poor shielding            |
    |                        | effect of d & f orbitals                                                     |
    |------------------------+------------------------------------------------------------------------------|
    
### Periodic Trends :
    
    |---------------------------+----------------------------------------------------------------------|
    | increasing down the group | reason                                                               |
    |---------------------------+----------------------------------------------------------------------|
    | radii/size                | increasing number of shell on outermost shell decreases              |
    | electron gain enthalpy    | increasing size of atom & decreasing effective nuclear charge        |
    | acidity                   | increasing size of atom & decreasing effective nuclear charge        |
    | reducing power            | increasing size of atom & decreasing effective nuclear charge        |
    | boiling point             | increasing van der waals forces due to increase in size of molecules |
    |---------------------------+----------------------------------------------------------------------|
    
    ◆ FH & H₂O has high boiling point due to H-bonding
    ◆ H₂O has higher boiling point than FH , H₂O has double amount of H as compared
      to FH resulting in larger amount of intermolecular H-bonding
    
    |-------------------------------+-----------------------------------------------------------------------|
    | decreasing down the group     | reason                                                                |
    |-------------------------------+-----------------------------------------------------------------------|
    | effective nuclear charge      | increasing distance between nucleus valence e- due to increasing size |
    | bond dissociation enthalpy    | increasing size of atom & decreasing effective nuclear charge         |
    | ionisation enthalpy           | increasing size of atom & decreasing effective nuclear charge         |
    | oxidising power               | increasing size of atom & decreasing effective nuclear charge         |
    | thermal stability of hydrides | increasing size of central atom, results in weaker H-bonding          |
    |-------------------------------+-----------------------------------------------------------------------|
    
    ◆ across the periodic table acidity increases with electronegativity & polarity
      example: P < S < Cl         order of electronegativity
               PH₃ < H₂S < HCl    order of acidic strength
    
## p-block :
### group 16 : O S Se Te Po

    ▶ ozone:
         - O₃ acts as powerful oxidising agent as it can easily liberate nascent oxygen [O]
         - in O₃ both O-O bond lengths are identical due to resonance
         - ozone is thermodynamically unstable because its decomposition into oxygen results in 
           liberation of heat  and  increased entropy. Hence, ozone can be explosive
       
    ▶ oxygen:
         - O₂ is paramgnetic (unpaired e-) in vapour state
         - due to small size O₂ suffers lp-lp repulsion & decreased van der walls force, O-O 
           bond becomes weak. Thus O₂ exists as gas & shows less tendency for catenation

    ▶ sulphur:
         - S₂ is paramgnetic (unpaired e-) in vapour state
         - van der waals forces of sulpher is higher due to its larger size thus it exists as 
           solid & shows higher tendency for catenation
         - S₈(rhombic sulpher) is stable at room temperature
         - S₈ when heated above 95⁰C become monoclinic sulphur
         - manufacturing of sulphur is favoured by low temperature & high pressure

    ◆ reducing character of of dioxides decreases from SO₂ to TeO₂ due to inert pair effect 
    ◆ SF₆ is kinetically stable due to steric hindrance
    ◆ H₂S is more acidic than PH₃ because S is more electronegative than P
        
<p align="center">
    <a> <img src=S+O.jpg ></a>
</p> 

### group 17: F Cl Br I At    (halogens)

     - small radii due to max effective nuclear charge 
     - high ionisation enthalpy because of max effective nuclear charge
     - coloured as they absorbs radiations in visible range due to the presence of unpaired e- 
     - strongest oxidising agents due to higher electronegativity
     - least electron gain enthalpy as they need 1 e- to become stable

     ▶ flourine:
     	- very small size
     	- strongest oxidising agent
     	- most electronegative element
     	- only exhibits OS(oxidation state) of -1 
		results :
 		it even oxidises other halides & forms most interhalogen compounds
 		forms only one oxoacid HOF
     	- suffers interelectronic repulsions due to its small size 
		results :
	        electron gain enthalpy F < Cl
	        dissociation enthalpy F-F < Cl-Cl
    ◆ CL₂ + F₂ → ClF₃ not FCl₃ because F only shows OS(oxidation state) of -1

    ◆ Deacon's process:     4HCl + O2 → 2Cl₂ + 2H₂O
                            H₂ + Cl₂ → 2HCL
                            I₂ + 6H₂O + 5Cl₂ → 2HIO₃ + 10HCl
    ◆ bleaching reaction:   coloured substance + [O] → colourless substance
    ◆ aqua regia :          mixture of conc. HCl & conc. HNO₃ (3:1 ratio)
    
            acidity / reducing power -------------> increases
            (oxoacids)               HClO HClO2 HClO3 ...  
            basicity / oxidising power -----------> decreases
   
    
## d-block:	(transition metals)
    - paramagnetic
    - exhibits metallic properties
    - variable OS due to similar energy of consecutive d s orbitals
    - used as catalyst due to variable OS
    - forms coloured compounds as they absorbs radiations in visible range due to d-d transition
    - forms complexes due to smaller size & vacant d orbital
    - Fe Co Ni (group 8 9 10) are of similar size due to repulsion of e- in d orbitals
    - forms alloy due to similar size of atoms
    - basic at lowest OS
    - amphoteric at intermidiate OS
    - acidic at highest OS
    
    ▶ Copper (Cu):  looses 1 e- to become stable 
      result:
           low ionisation energy
           only d-block element to show +1 OS
    
    ▶ Zinc (zn):  not considered as transition element due to completely filled d orbital & +2 OS
    
    ▶ Manganese (Mn):  Mn has max number of unpaired e- in d orbital & e- from both s d orbital 
                     can take part in bond formation. Thus it shows +7 OS
    
    ▶ potassium dichromate:
    	4FeCr₂O₇ + 8Na₂CO₃ + 7O₂ → 8Na₂CrO₄
    	2Na₂Cr₂O₄ + 2H → NaCr₂O₇
    	NaCr₂O₇ + 2KCl → K₂Cr₂O₇ + 2NaCl
    	- colour of it depends on pH of solution
    	- used in volumetric analysis
    	- strong oxidising agent
    
    ◆ disproportionation reaction:  reaction in which same substance get oxidised & reduced
      example: 2Cu⁺ → Cu²⁺ + Cu
        
## f-block:
 
    |--------------------------------------------------------+------------------------------------------------------|
    | lanthanides                                            | actinides                                            |
    |--------------------------------------------------------+------------------------------------------------------|
    | common OS +3                                           | common OS +3                                         |
    | diamagnetic                                            | paramagnetic                                         |
    | lower tendency to form complexes                       | higher tendency to form complexes                    |
    | lanthanides contraction:                               | actinides contraction:                               |
    | due to poor shielding effect of d & f orbitals         | due to poor shielding effect of d & f orbitals       |
    | steady decrease in radii due to lanthanoid contraction | steady decrease in radii due to actinide contraction |
    | coloured because of f-f transition                     | variable OS due to similar energy of f d s orbitals  |
    |--------------------------------------------------------+------------------------------------------------------|

#### Xenon (Xe) reactions:
    XeF₆ + 3H₂O → XeO₃ + 6HF
    XeF₄ + O₂F₂ → XeF₆ + O₂
    2XeF₂ + 2H₂O → 2Xe + 4HF + O₂
    XeF₂ + PF₃ → [XeF]⁺ [PF₄]⁻
    XeF₄ + SbF₅ → [XeF₃]⁺ [SbF₆]⁻
    XeF₆ + MF → M⁺ [XeF₇]⁻
